#ifndef FIGURE_H
#define FIGURE_H
#include <string>
#include <vector>
#include "../Point/Point.h"
using namespace std;


class Figure{
public:
    Figure():name(""){};
    virtual void clear() = 0;

    string name;
};

class LoadFigure:public Figure{
public:
    void clear(){ name="";angles.clear();side_sz.clear(); }
    vector<int> angles;
    vector<double> side_sz;
};

class LogicFigure:public Figure{
public:
    void clear(){ name="";vertex.clear(); }
    vector<Point> vertex;
};

#endif // FIGURE_H
