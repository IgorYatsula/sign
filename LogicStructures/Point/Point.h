#ifndef POINT_H
#define POINT_H

#include "../openGL/glut.h"

struct Point{
    GLdouble x,y;
    Point(GLdouble _x, GLdouble _y):x(_x),y(_y){};
    Point(){x=y=-1;}
    Point& operator =(const Point& p)
    {
        x = p.x;
        y = p.y;
        return *this;
    }
};

#endif // POINT_H
