#include<cmath>
#include "../Point/Point.h"
#include "math.h"

double getDistance(const Point p1, const Point p2)
{
    return sqrt( pow(p1.x-p2.x,2) + pow(p1.y-p2.y,2) );
}

double _abs(const double dig)
{
    return (dig<0) ? -dig:dig;
}

int _angle(const double COS)
{
    double F_COS;
    int res = 0;
    int qu = (COS<0) ? -1:1;
    for(int i=0;i<=90;i++)
    {
        F_COS = cos(i/180.0 * M_PI);
        if( F_COS-0.01<=qu*COS && F_COS+0.01>=qu*COS) {res=i; break;}
    }
    return (qu==-1) ? 180-res:res;
}


int countAngle(const Point _a,const Point _b,const  Point _c)
{
    double a = getDistance(_a,_b);
    double b = getDistance(_b,_c);
    double c = getDistance(_c,_a);

    double COS_ALPHA = (pow(a,2)+pow(b,2)-pow(c,2)) / (2*a*b);
    return _angle(COS_ALPHA);
}
